/**
 */
package tdt4250.spp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.spp.Institute;
import tdt4250.spp.SppFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Institute</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstituteTest extends TestCase {

	/**
	 * The fixture for this Institute test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Institute fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InstituteTest.class);
	}

	/**
	 * Constructs a new Institute test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstituteTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Institute test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Institute fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Institute test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Institute getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SppFactory.eINSTANCE.createInstitute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InstituteTest
