/**
 */
package tdt4250.spp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.spp.Course;
import tdt4250.spp.LevelType;
import tdt4250.spp.Program;
import tdt4250.spp.Semester;
import tdt4250.spp.SppFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.spp.Program#getNumberOfSemesters() <em>Number Of Semesters</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.spp.Program#getCredits() <em>Get Credits</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ProgramTest extends TestCase {

	/**
	 * The fixture for this Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Program fixture = null;
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProgramTest.class);
	}

	/**
	 * Constructs a new Program test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProgramTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Program fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Program getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SppFactory.eINSTANCE.createProgram());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.spp.Program#getNumberOfSemesters() <em>Number Of Semesters</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.spp.Program#getNumberOfSemesters()
	 * @generated
	 */
	public void testGetNumberOfSemesters() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link tdt4250.spp.Program#getNumberOfSemesters() <em>Number Of Semesters</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.spp.Program#getNumberOfSemesters()
	 * @generated NOPE
	 */
	public void testGetNumberOfSemestersForMastersProgram() {
		Program program = SppFactory.eINSTANCE.createProgram();
		program.setDuration(LevelType.MASTER);
		
		int expectedNumberOfSemesters = 4;
		
		assertEquals(expectedNumberOfSemesters, program.getNumberOfSemesters());
	}
	
	public void testGetNumberOfSemestersForIntegratedMastersProgram() {
		Program program = SppFactory.eINSTANCE.createProgram();
		program.setDuration(LevelType.INTEGRATED_MASTERS);
		
		int expectedNumberOfSemesters = 10;
		
		assertEquals(expectedNumberOfSemesters, program.getNumberOfSemesters());
	}
	
	public void testGetNumberOfSemestersForBachelorProgram() {
		Program program = SppFactory.eINSTANCE.createProgram();
		program.setDuration(LevelType.BACHELOR);
		
		int expectedNumberOfSemesters = 6;
		
		assertEquals(expectedNumberOfSemesters, program.getNumberOfSemesters());
	}

	/**
	 * Tests the '{@link tdt4250.spp.Program#getCredits() <em>Get Credits</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.spp.Program#getCredits()
	 * @generated NOPE
	 */
	public void testGetCredits() {
		Program program = getFixture();
		float actualNumberOfCredits = program.getCredits();
		float expectedNumberOfCredits = 0.0f;
		for (Semester semester : program.getSemesters()) {
			for (Course course : semester.getSelectedCourses()) {
				expectedNumberOfCredits += course.getCredits();
			}
		}
		
		assertEquals(expectedNumberOfCredits, actualNumberOfCredits);
	}

} //ProgramTest
