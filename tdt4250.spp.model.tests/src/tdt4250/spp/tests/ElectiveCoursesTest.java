/**
 */
package tdt4250.spp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.spp.ElectiveCourses;
import tdt4250.spp.SppFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Elective Courses</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ElectiveCoursesTest extends TestCase {

	/**
	 * The fixture for this Elective Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectiveCourses fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ElectiveCoursesTest.class);
	}

	/**
	 * Constructs a new Elective Courses test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElectiveCoursesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Elective Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ElectiveCourses fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Elective Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectiveCourses getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SppFactory.eINSTANCE.createElectiveCourses());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ElectiveCoursesTest
