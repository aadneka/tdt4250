/**
 */
package tdt4250.spp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.spp.MandatoryCourses;
import tdt4250.spp.SppFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Mandatory Courses</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MandatoryCoursesTest extends TestCase {

	/**
	 * The fixture for this Mandatory Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MandatoryCourses fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MandatoryCoursesTest.class);
	}

	/**
	 * Constructs a new Mandatory Courses test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MandatoryCoursesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Mandatory Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MandatoryCourses fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Mandatory Courses test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MandatoryCourses getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SppFactory.eINSTANCE.createMandatoryCourses());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MandatoryCoursesTest
