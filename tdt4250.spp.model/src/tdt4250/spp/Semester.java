/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.Semester#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250.spp.Semester#getSeason <em>Season</em>}</li>
 *   <li>{@link tdt4250.spp.Semester#getProgram <em>Program</em>}</li>
 *   <li>{@link tdt4250.spp.Semester#getSelectedCourses <em>Selected Courses</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsEnoughCredits coursesNeedsToBeMandatoryOrElectiveCourses'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 coursesNeedsToBeMandatoryOrElectiveCourses='((self.selectedCourses-&gt;intersection(self.eContainer().mandatoryCourses.courses))-&gt;notEmpty() or (self.selectedCourses-&gt;intersection(self.eContainer().electiveCourses.courses))-&gt;notEmpty()) or self.selectedCourses -&gt; isEmpty()'"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see #setSemester(Integer)
	 * @see tdt4250.spp.SppPackage#getSemester_Semester()
	 * @model default="1"
	 * @generated
	 */
	Integer getSemester();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Semester#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(Integer value);

	/**
	 * Returns the value of the '<em><b>Season</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.spp.SeasonType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Season</em>' attribute.
	 * @see tdt4250.spp.SeasonType
	 * @see #setSeason(SeasonType)
	 * @see tdt4250.spp.SppPackage#getSemester_Season()
	 * @model
	 * @generated
	 */
	SeasonType getSeason();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Semester#getSeason <em>Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Season</em>' attribute.
	 * @see tdt4250.spp.SeasonType
	 * @see #getSeason()
	 * @generated
	 */
	void setSeason(SeasonType value);

	/**
	 * Returns the value of the '<em><b>Program</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Program#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' container reference.
	 * @see #setProgram(Program)
	 * @see tdt4250.spp.SppPackage#getSemester_Program()
	 * @see tdt4250.spp.Program#getSemesters
	 * @model opposite="semesters" transient="false"
	 * @generated
	 */
	Program getProgram();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Semester#getProgram <em>Program</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' container reference.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(Program value);

	/**
	 * Returns the value of the '<em><b>Selected Courses</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.spp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Courses</em>' reference list.
	 * @see tdt4250.spp.SppPackage#getSemester_SelectedCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getSelectedCourses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	float getCredits();

} // Semester
