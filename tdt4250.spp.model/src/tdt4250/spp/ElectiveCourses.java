/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elective Courses</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.ElectiveCourses#getProgram <em>Program</em>}</li>
 *   <li>{@link tdt4250.spp.ElectiveCourses#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getElectiveCourses()
 * @model
 * @generated
 */
public interface ElectiveCourses extends EObject {
	/**
	 * Returns the value of the '<em><b>Program</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Program#getElectiveCourses <em>Elective Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' container reference.
	 * @see #setProgram(Program)
	 * @see tdt4250.spp.SppPackage#getElectiveCourses_Program()
	 * @see tdt4250.spp.Program#getElectiveCourses
	 * @model opposite="electiveCourses" transient="false"
	 * @generated
	 */
	Program getProgram();

	/**
	 * Sets the value of the '{@link tdt4250.spp.ElectiveCourses#getProgram <em>Program</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' container reference.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(Program value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.spp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see tdt4250.spp.SppPackage#getElectiveCourses_Courses()
	 * @model
	 * @generated
	 */
	EList<Course> getCourses();

} // ElectiveCourses
