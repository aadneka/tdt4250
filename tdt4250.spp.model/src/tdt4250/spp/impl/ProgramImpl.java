/**
 */
package tdt4250.spp.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.spp.ElectiveCourses;
import tdt4250.spp.LevelType;
import tdt4250.spp.MandatoryCourses;
import tdt4250.spp.Program;
import tdt4250.spp.Semester;
import tdt4250.spp.Specialisation;
import tdt4250.spp.SppPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getSemesters <em>Semesters</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getSpecialisation <em>Specialisation</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link tdt4250.spp.impl.ProgramImpl#getNumberOfSemesters <em>Number Of Semesters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProgramImpl extends MinimalEObjectImpl.Container implements Program {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final LevelType DURATION_EDEFAULT = LevelType.BACHELOR;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected LevelType duration = DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSemesters() <em>Semesters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesters()
	 * @generated
	 * @ordered
	 */
	protected EList<Semester> semesters;

	/**
	 * The cached value of the '{@link #getSpecialisation() <em>Specialisation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialisation()
	 * @generated
	 * @ordered
	 */
	protected EList<Specialisation> specialisation;

	/**
	 * The cached value of the '{@link #getMandatoryCourses() <em>Mandatory Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatoryCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<MandatoryCourses> mandatoryCourses;

	/**
	 * The cached value of the '{@link #getElectiveCourses() <em>Elective Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<ElectiveCourses> electiveCourses;

	/**
	 * The default value of the '{@link #getNumberOfSemesters() <em>Number Of Semesters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfSemesters()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_SEMESTERS_EDEFAULT = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SppPackage.Literals.PROGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SppPackage.PROGRAM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LevelType getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDuration(LevelType newDuration) {
		LevelType oldDuration = duration;
		duration = newDuration == null ? DURATION_EDEFAULT : newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SppPackage.PROGRAM__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Semester> getSemesters() {
		if (semesters == null) {
			semesters = new EObjectContainmentWithInverseEList<Semester>(Semester.class, this, SppPackage.PROGRAM__SEMESTERS, SppPackage.SEMESTER__PROGRAM);
		}
		return semesters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialisation> getSpecialisation() {
		if (specialisation == null) {
			specialisation = new EObjectContainmentWithInverseEList<Specialisation>(Specialisation.class, this, SppPackage.PROGRAM__SPECIALISATION, SppPackage.SPECIALISATION__PROGRAM);
		}
		return specialisation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MandatoryCourses> getMandatoryCourses() {
		if (mandatoryCourses == null) {
			mandatoryCourses = new EObjectContainmentWithInverseEList<MandatoryCourses>(MandatoryCourses.class, this, SppPackage.PROGRAM__MANDATORY_COURSES, SppPackage.MANDATORY_COURSES__PROGRAM);
		}
		return mandatoryCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ElectiveCourses> getElectiveCourses() {
		if (electiveCourses == null) {
			electiveCourses = new EObjectContainmentWithInverseEList<ElectiveCourses>(ElectiveCourses.class, this, SppPackage.PROGRAM__ELECTIVE_COURSES, SppPackage.ELECTIVE_COURSES__PROGRAM);
		}
		return electiveCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * The number of semesters should be equal to the duration multiplied by two semesters.
	 * <!-- end-user-doc -->
	 * @generated NOPE
	 */
	@Override
	public int getNumberOfSemesters() {
		return this.duration.getValue() * 2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Get credits for all semesters
	 * <!-- end-user-doc -->
	 * @generated NOPE
	 */
	@Override
	public float getCredits() {
		float result = 0.0f;
		for (Semester sem : this.getSemesters()) {
			result += sem.getCredits();
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SppPackage.PROGRAM__SEMESTERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSemesters()).basicAdd(otherEnd, msgs);
			case SppPackage.PROGRAM__SPECIALISATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecialisation()).basicAdd(otherEnd, msgs);
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMandatoryCourses()).basicAdd(otherEnd, msgs);
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getElectiveCourses()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SppPackage.PROGRAM__SEMESTERS:
				return ((InternalEList<?>)getSemesters()).basicRemove(otherEnd, msgs);
			case SppPackage.PROGRAM__SPECIALISATION:
				return ((InternalEList<?>)getSpecialisation()).basicRemove(otherEnd, msgs);
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				return ((InternalEList<?>)getMandatoryCourses()).basicRemove(otherEnd, msgs);
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				return ((InternalEList<?>)getElectiveCourses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SppPackage.PROGRAM__NAME:
				return getName();
			case SppPackage.PROGRAM__DURATION:
				return getDuration();
			case SppPackage.PROGRAM__SEMESTERS:
				return getSemesters();
			case SppPackage.PROGRAM__SPECIALISATION:
				return getSpecialisation();
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				return getMandatoryCourses();
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				return getElectiveCourses();
			case SppPackage.PROGRAM__NUMBER_OF_SEMESTERS:
				return getNumberOfSemesters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SppPackage.PROGRAM__NAME:
				setName((String)newValue);
				return;
			case SppPackage.PROGRAM__DURATION:
				setDuration((LevelType)newValue);
				return;
			case SppPackage.PROGRAM__SEMESTERS:
				getSemesters().clear();
				getSemesters().addAll((Collection<? extends Semester>)newValue);
				return;
			case SppPackage.PROGRAM__SPECIALISATION:
				getSpecialisation().clear();
				getSpecialisation().addAll((Collection<? extends Specialisation>)newValue);
				return;
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				getMandatoryCourses().clear();
				getMandatoryCourses().addAll((Collection<? extends MandatoryCourses>)newValue);
				return;
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				getElectiveCourses().clear();
				getElectiveCourses().addAll((Collection<? extends ElectiveCourses>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SppPackage.PROGRAM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SppPackage.PROGRAM__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case SppPackage.PROGRAM__SEMESTERS:
				getSemesters().clear();
				return;
			case SppPackage.PROGRAM__SPECIALISATION:
				getSpecialisation().clear();
				return;
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				getMandatoryCourses().clear();
				return;
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				getElectiveCourses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SppPackage.PROGRAM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SppPackage.PROGRAM__DURATION:
				return duration != DURATION_EDEFAULT;
			case SppPackage.PROGRAM__SEMESTERS:
				return semesters != null && !semesters.isEmpty();
			case SppPackage.PROGRAM__SPECIALISATION:
				return specialisation != null && !specialisation.isEmpty();
			case SppPackage.PROGRAM__MANDATORY_COURSES:
				return mandatoryCourses != null && !mandatoryCourses.isEmpty();
			case SppPackage.PROGRAM__ELECTIVE_COURSES:
				return electiveCourses != null && !electiveCourses.isEmpty();
			case SppPackage.PROGRAM__NUMBER_OF_SEMESTERS:
				return getNumberOfSemesters() != NUMBER_OF_SEMESTERS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SppPackage.PROGRAM___GET_CREDITS:
				return getCredits();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //ProgramImpl
