/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Institute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.Institute#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.spp.Institute#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250.spp.Institute#getUniversity <em>University</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getInstitute()
 * @model
 * @generated
 */
public interface Institute extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.spp.SppPackage#getInstitute_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Institute#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Course#getResponsibleUnit <em>Responsible Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getInstitute_Courses()
	 * @see tdt4250.spp.Course#getResponsibleUnit
	 * @model opposite="responsibleUnit" containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>University</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.University#getInstitutes <em>Institutes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>University</em>' container reference.
	 * @see #setUniversity(University)
	 * @see tdt4250.spp.SppPackage#getInstitute_University()
	 * @see tdt4250.spp.University#getInstitutes
	 * @model opposite="institutes" transient="false"
	 * @generated
	 */
	University getUniversity();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Institute#getUniversity <em>University</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>University</em>' container reference.
	 * @see #getUniversity()
	 * @generated
	 */
	void setUniversity(University value);

} // Institute
