/**
 */
package tdt4250.spp.util;

import java.util.Map;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import tdt4250.spp.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.spp.SppPackage
 * @generated
 */
public class SppValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final SppValidator INSTANCE = new SppValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.spp";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SppValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return SppPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case SppPackage.UNIVERSITY:
				return validateUniversity((University)value, diagnostics, context);
			case SppPackage.INSTITUTE:
				return validateInstitute((Institute)value, diagnostics, context);
			case SppPackage.PROGRAM:
				return validateProgram((Program)value, diagnostics, context);
			case SppPackage.SEMESTER:
				return validateSemester((Semester)value, diagnostics, context);
			case SppPackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case SppPackage.SPECIALISATION:
				return validateSpecialisation((Specialisation)value, diagnostics, context);
			case SppPackage.MANDATORY_COURSES:
				return validateMandatoryCourses((MandatoryCourses)value, diagnostics, context);
			case SppPackage.ELECTIVE_COURSES:
				return validateElectiveCourses((ElectiveCourses)value, diagnostics, context);
			case SppPackage.LEVEL_TYPE:
				return validateLevelType((LevelType)value, diagnostics, context);
			case SppPackage.SEASON_TYPE:
				return validateSeasonType((SeasonType)value, diagnostics, context);
			case SppPackage.COURSE_LEVEL_TYPE:
				return validateCourseLevelType((CourseLevelType)value, diagnostics, context);
			case SppPackage.COURSE_CODE:
				return validateCourseCode((String)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(university, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInstitute(Institute institute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(institute, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProgram(Program program, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(program, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(program, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(program, diagnostics, context);
		if (result || diagnostics != null) result &= validateProgram_needsEnoughSemesters(program, diagnostics, context);
		if (result || diagnostics != null) result &= validateProgram_needsEnoughCredits(program, diagnostics, context);
		return result;
	}

	/**
	 * Validates the needsEnoughSemesters constraint of '<em>Program</em>'.
	 * <!-- begin-user-doc -->
	 * The number of semesters should equal the Duration of program timed by two semesters per year.
	 * <!-- end-user-doc -->
	 * @generated NOPE
	 */
	public boolean validateProgram_needsEnoughSemesters(Program program, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int numberOfSemesters = 0;
		EList<Semester> semesters = program.getSemesters();
		for (int i = 0; i < semesters.size(); i++) {
			numberOfSemesters += 1;
		}
		
		if (numberOfSemesters < program.getDuration().getValue() * 2) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "needsEnoughSemesters", getObjectLabel(program, context) },
						 new Object[] { program },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the needsEnoughCredits constraint of '<em>Program</em>'.
	 * <!-- begin-user-doc -->
	 * We assume that the program has enough credits as long as the sum of semesters multiplied by 30 credits are fulfilled..
	 * That means that in theory this validation does not care if one semester has 15 credits, as long as the average is 30 or more.
	 * <!-- end-user-doc -->
	 * @generated NOPE
	 */
	public boolean validateProgram_needsEnoughCredits(Program program, DiagnosticChain diagnostics, Map<Object, Object> context) {
		float credits = 0.0f;
		for (Semester sem : program.getSemesters()) {
			credits += sem.getCredits();
		}
		
		if (credits < program.getNumberOfSemesters() * 30.0f) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "needsEnoughCredits", getObjectLabel(program, context) },
						 new Object[] { program },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(semester, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validateSemester_needsEnoughCredits(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validateSemester_coursesNeedsToBeMandatoryOrElectiveCourses(semester, diagnostics, context);
		return result;
	}

	/**
	 * Validates the needsEnoughCredits constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * To be able to have a valid semester the semester should at least have 30 credits.
	 * <!-- end-user-doc -->
	 * @generated NOPE
	 */
	public boolean validateSemester_needsEnoughCredits(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (semester.getCredits() < 30.0f) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "needsEnoughCredits", getObjectLabel(semester, context) },
						 new Object[] { semester },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the coursesNeedsToBeMandatoryOrElectiveCourses constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SEMESTER__COURSES_NEEDS_TO_BE_MANDATORY_OR_ELECTIVE_COURSES__EEXPRESSION = "((self.selectedCourses->intersection(self.eContainer().mandatoryCourses.courses))->notEmpty() or (self.selectedCourses->intersection(self.eContainer().electiveCourses.courses))->notEmpty()) or self.selectedCourses -> isEmpty()";

	/**
	 * Validates the coursesNeedsToBeMandatoryOrElectiveCourses constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester_coursesNeedsToBeMandatoryOrElectiveCourses(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SppPackage.Literals.SEMESTER,
				 semester,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "coursesNeedsToBeMandatoryOrElectiveCourses",
				 SEMESTER__COURSES_NEEDS_TO_BE_MANDATORY_OR_ELECTIVE_COURSES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(course, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_courseCodeNeedsToBeUnique(course, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the courseCodeNeedsToBeUnique constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__COURSE_CODE_NEEDS_TO_BE_UNIQUE__EEXPRESSION = "self.eContainer().eContainer().institutes.courses.code -> select(i: String | i = self.code) -> size() < 2";

	/**
	 * Validates the courseCodeNeedsToBeUnique constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_courseCodeNeedsToBeUnique(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SppPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "courseCodeNeedsToBeUnique",
				 COURSE__COURSE_CODE_NEEDS_TO_BE_UNIQUE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSpecialisation(Specialisation specialisation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(specialisation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMandatoryCourses(MandatoryCourses mandatoryCourses, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(mandatoryCourses, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(mandatoryCourses, diagnostics, context);
		if (result || diagnostics != null) result &= validateMandatoryCourses_needsToHaveEmptyIntersectionWithElectiveCourses(mandatoryCourses, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the needsToHaveEmptyIntersectionWithElectiveCourses constraint of '<em>Mandatory Courses</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MANDATORY_COURSES__NEEDS_TO_HAVE_EMPTY_INTERSECTION_WITH_ELECTIVE_COURSES__EEXPRESSION = "self.eContainer().electiveCourses.courses->intersection(self.courses)->isEmpty()";

	/**
	 * Validates the needsToHaveEmptyIntersectionWithElectiveCourses constraint of '<em>Mandatory Courses</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMandatoryCourses_needsToHaveEmptyIntersectionWithElectiveCourses(MandatoryCourses mandatoryCourses, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SppPackage.Literals.MANDATORY_COURSES,
				 mandatoryCourses,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "needsToHaveEmptyIntersectionWithElectiveCourses",
				 MANDATORY_COURSES__NEEDS_TO_HAVE_EMPTY_INTERSECTION_WITH_ELECTIVE_COURSES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElectiveCourses(ElectiveCourses electiveCourses, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(electiveCourses, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLevelType(LevelType levelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSeasonType(SeasonType seasonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseLevelType(CourseLevelType courseLevelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseCode(String courseCode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //SppValidator
