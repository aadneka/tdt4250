/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mandatory Courses</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.MandatoryCourses#getProgram <em>Program</em>}</li>
 *   <li>{@link tdt4250.spp.MandatoryCourses#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getMandatoryCourses()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsToHaveEmptyIntersectionWithElectiveCourses'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 needsToHaveEmptyIntersectionWithElectiveCourses='self.eContainer().electiveCourses.courses-&gt;intersection(self.courses)-&gt;isEmpty()'"
 * @generated
 */
public interface MandatoryCourses extends EObject {
	/**
	 * Returns the value of the '<em><b>Program</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Program#getMandatoryCourses <em>Mandatory Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' container reference.
	 * @see #setProgram(Program)
	 * @see tdt4250.spp.SppPackage#getMandatoryCourses_Program()
	 * @see tdt4250.spp.Program#getMandatoryCourses
	 * @model opposite="mandatoryCourses" transient="false"
	 * @generated
	 */
	Program getProgram();

	/**
	 * Sets the value of the '{@link tdt4250.spp.MandatoryCourses#getProgram <em>Program</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' container reference.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(Program value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.spp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see tdt4250.spp.SppPackage#getMandatoryCourses_Courses()
	 * @model
	 * @generated
	 */
	EList<Course> getCourses();

} // MandatoryCourses
