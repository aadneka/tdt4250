/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.University#getPrograms <em>Programs</em>}</li>
 *   <li>{@link tdt4250.spp.University#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.spp.University#getInstitutes <em>Institutes</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends EObject {
	/**
	 * Returns the value of the '<em><b>Programs</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.Program}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programs</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getUniversity_Programs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Program> getPrograms();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.spp.SppPackage#getUniversity_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.spp.University#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Institutes</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.Institute}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Institute#getUniversity <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institutes</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getUniversity_Institutes()
	 * @see tdt4250.spp.Institute#getUniversity
	 * @model opposite="university" containment="true"
	 * @generated
	 */
	EList<Institute> getInstitutes();

} // University
