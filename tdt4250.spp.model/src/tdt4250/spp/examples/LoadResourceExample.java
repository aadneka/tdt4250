package tdt4250.spp.examples;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import tdt4250.spp.Program;
import tdt4250.spp.Semester;
import tdt4250.spp.SppPackage;
import tdt4250.spp.University;

public class LoadResourceExample {
	
	public static void main(String[] args) {
		ResourceSet resSet = new ResourceSetImpl();
		
		resSet.getPackageRegistry().put(SppPackage.eNS_URI, SppPackage.eINSTANCE);
		
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		
		Resource resource = resSet.getResource(URI.createURI(RunValidationExample.class.getResource("RunValidationExample.xmi").toString()), true);
		University uni = (University) resource.getContents().get(0);
		
		for (Program program : uni.getPrograms()) {
			for (Semester sem : program.getSemesters()) {
				System.out.println("Number of credits for semester " + sem.getSemester() + " " + sem.getSeason() + " of program " + program.getName() + ": " + sem.getCredits());
			}
		}
	}
	

}
