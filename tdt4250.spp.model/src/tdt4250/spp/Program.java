/**
 */
package tdt4250.spp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.Program#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getDuration <em>Duration</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getSemesters <em>Semesters</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getSpecialisation <em>Specialisation</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getMandatoryCourses <em>Mandatory Courses</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getElectiveCourses <em>Elective Courses</em>}</li>
 *   <li>{@link tdt4250.spp.Program#getNumberOfSemesters <em>Number Of Semesters</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getProgram()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsEnoughSemesters needsEnoughCredits'"
 * @generated
 */
public interface Program extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.spp.SppPackage#getProgram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Program#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.spp.LevelType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see tdt4250.spp.LevelType
	 * @see #setDuration(LevelType)
	 * @see tdt4250.spp.SppPackage#getProgram_Duration()
	 * @model
	 * @generated
	 */
	LevelType getDuration();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Program#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see tdt4250.spp.LevelType
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(LevelType value);

	/**
	 * Returns the value of the '<em><b>Semesters</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.Semester}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Semester#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semesters</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getProgram_Semesters()
	 * @see tdt4250.spp.Semester#getProgram
	 * @model opposite="program" containment="true"
	 * @generated
	 */
	EList<Semester> getSemesters();

	/**
	 * Returns the value of the '<em><b>Specialisation</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.Specialisation}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Specialisation#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisation</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getProgram_Specialisation()
	 * @see tdt4250.spp.Specialisation#getProgram
	 * @model opposite="program" containment="true" upper="20"
	 * @generated
	 */
	EList<Specialisation> getSpecialisation();

	/**
	 * Returns the value of the '<em><b>Mandatory Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.MandatoryCourses}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.MandatoryCourses#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory Courses</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getProgram_MandatoryCourses()
	 * @see tdt4250.spp.MandatoryCourses#getProgram
	 * @model opposite="program" containment="true"
	 * @generated
	 */
	EList<MandatoryCourses> getMandatoryCourses();

	/**
	 * Returns the value of the '<em><b>Elective Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.spp.ElectiveCourses}.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.ElectiveCourses#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Courses</em>' containment reference list.
	 * @see tdt4250.spp.SppPackage#getProgram_ElectiveCourses()
	 * @see tdt4250.spp.ElectiveCourses#getProgram
	 * @model opposite="program" containment="true"
	 * @generated
	 */
	EList<ElectiveCourses> getElectiveCourses();

	/**
	 * Returns the value of the '<em><b>Number Of Semesters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Semesters</em>' attribute.
	 * @see tdt4250.spp.SppPackage#getProgram_NumberOfSemesters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getNumberOfSemesters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	float getCredits();

} // Program
