/**
 */
package tdt4250.spp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.spp.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.spp.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.spp.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.spp.Course#getLevel <em>Level</em>}</li>
 *   <li>{@link tdt4250.spp.Course#getSeason <em>Season</em>}</li>
 *   <li>{@link tdt4250.spp.Course#getResponsibleUnit <em>Responsible Unit</em>}</li>
 * </ul>
 *
 * @see tdt4250.spp.SppPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='courseCodeNeedsToBeUnique'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 courseCodeNeedsToBeUnique='self.eContainer().eContainer().institutes.courses.code -&gt; select(i: String | i = self.code) -&gt; size() &lt; 2'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see tdt4250.spp.SppPackage#getCourse_Code()
	 * @model dataType="tdt4250.spp.CourseCode"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.spp.SppPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(float)
	 * @see tdt4250.spp.SppPackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	float getCredits();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(float value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.spp.CourseLevelType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see tdt4250.spp.CourseLevelType
	 * @see #setLevel(CourseLevelType)
	 * @see tdt4250.spp.SppPackage#getCourse_Level()
	 * @model
	 * @generated
	 */
	CourseLevelType getLevel();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see tdt4250.spp.CourseLevelType
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(CourseLevelType value);

	/**
	 * Returns the value of the '<em><b>Season</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.spp.SeasonType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Season</em>' attribute.
	 * @see tdt4250.spp.SeasonType
	 * @see #setSeason(SeasonType)
	 * @see tdt4250.spp.SppPackage#getCourse_Season()
	 * @model
	 * @generated
	 */
	SeasonType getSeason();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getSeason <em>Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Season</em>' attribute.
	 * @see tdt4250.spp.SeasonType
	 * @see #getSeason()
	 * @generated
	 */
	void setSeason(SeasonType value);

	/**
	 * Returns the value of the '<em><b>Responsible Unit</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.spp.Institute#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Unit</em>' container reference.
	 * @see #setResponsibleUnit(Institute)
	 * @see tdt4250.spp.SppPackage#getCourse_ResponsibleUnit()
	 * @see tdt4250.spp.Institute#getCourses
	 * @model opposite="courses" transient="false"
	 * @generated
	 */
	Institute getResponsibleUnit();

	/**
	 * Sets the value of the '{@link tdt4250.spp.Course#getResponsibleUnit <em>Responsible Unit</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible Unit</em>' container reference.
	 * @see #getResponsibleUnit()
	 * @generated
	 */
	void setResponsibleUnit(Institute value);

} // Course
